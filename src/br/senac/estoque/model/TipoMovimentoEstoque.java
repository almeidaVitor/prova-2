package br.senac.estoque.model;


public enum TipoMovimentoEstoque {
    
    ENTRADA('E'), SAIDA('S');
    
    private Character codigo;
    
    private TipoMovimentoEstoque(Character codigo){
        this.codigo = codigo;
    }

    public Character getCodigo() {
        return codigo;
    }
    
    public static TipoMovimentoEstoque getTipoPorCodigo(String codigo){
        if("E".equalsIgnoreCase(codigo) || "ENTRADA".equalsIgnoreCase(codigo))
            return TipoMovimentoEstoque.ENTRADA;
        else if("S".equalsIgnoreCase(codigo) || "SAIDA".equalsIgnoreCase(codigo))
            return TipoMovimentoEstoque.SAIDA;
        else
            return null;
    }
}
