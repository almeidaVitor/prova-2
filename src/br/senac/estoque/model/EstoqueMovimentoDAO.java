package br.senac.estoque.model;

import br.senac.estoque.model.EstoqueMovimento;
import br.senac.componente.model.BaseDAO;
import br.senac.componentes.db.ConexaoDB;
import br.senac.produto.model.Mercadoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EstoqueMovimentoDAO implements BaseDAO<EstoqueMovimento, Long>{
    
    
    EstoqueMovimento estoque = new EstoqueMovimento();
    
    /**
     * A partir de um ResultSet passado como parâmetro, cujo cursor está posicionado
     * em alguma linha da tabela estoquemovto, faça o DataBinding dos dados do ResultSet 
     * para um objeto do tipo EstoqueMovimento.
     * @param rs
     */
    private EstoqueMovimento getEstoqueMovimento(ResultSet rs) throws SQLException {
        
        EstoqueMovimento estoqueMov = new EstoqueMovimento();
        Mercadoria mercadoria = new Mercadoria();
        mercadoria.setIdMercadoria(rs.getLong("idMovtoEstoque"));
        estoqueMov.setProduto(mercadoria);
        estoqueMov.setQuantidade(rs.getDouble("quantidade"));
        estoqueMov.setTipoMovto(TipoMovimentoEstoque.getTipoPorCodigo(rs.getString("tipoMovto")));
        estoqueMov.setDataMovto(rs.getDate("dataMovto"));
        estoqueMov.getProduto().setIdProduto(rs.getLong("idProduto"));
        estoqueMov.setIdUsuario(rs.getInt("idUsuario"));
        estoqueMov.setObservacoes("observacoes");
        
        return estoqueMov;

    }
    

    /**
     * A partir do idMovtoEstoque, utilizando Statement ou PreparedStatement, retorne
     * o objeto do tipo EstoqueMovimento. Utilize o método getEstoqueMovimento para
     * fazer o DataBinding.
     * @param idMovtoEstoque
     */
    @Override
    public EstoqueMovimento getPorId(Long id) throws SQLException {

        String sql = "Select * from estoquemovto where idMovtoEstoque = " + id;
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery(sql);
        if (rs.next() == false) {
            throw new RuntimeException("Estoque movimento não encontrado: " + id + ".");
        }
        EstoqueMovimento estMov = getEstoqueMovimento(rs);
        return estMov;
    }


    /**
     * Utilizando Statement ou PreparedStatement atualize o Saldo de Estoque.
     * Se o produto não existir na tabela estoquesaldo, é necessário incluir um registro na
     * tabela estoquesaldo desse mesmo produto. Se o produto existir na tabela estoquesaldo, é necessário 
     * apenas somar a quantidade (se o parâmetro "quantidade" tiver valor positivo, vai aumentar a quantidade no estoque,
     * se for negativo, vai diminuir a quantidade no estoque).
     * @param idMovtoEstoque 
     */
    private void atualizarSaldoEstoque(Long idProduto, Double quantidade) throws SQLException{
        throw new UnsupportedOperationException("Não implementado ainda!");
    }


    /**
     * Utilize PreparedStatement. 
     * Após inserir (INSERT) um registro na tabela estoquemovto, atualize também a tabela
     * estoquesaldo através do método atualizarSaldoEstoque.    
     * Utilize transação, pois iremos atualizar dois registros de tabelas diferentes, 
     * caso ocorra qualquer exceção, faça o rollback.
     * Todos os campos são obrigatórios, com exceção dos campos idUsuario e observacoes que podem ou não ser nulos (null).
     * @param movtoEstoque
     */
    @Override
    public Long inserir(EstoqueMovimento movtoEstoque) throws SQLException {
        String sql = "insert into estoquemovto(quantidade, tipoMovto, dataMovto, idProduto, idUsuario, observacoes) values"
                + "?,?,?,?,?,? ";
        Connection conn = ConexaoDB.getInstance().getConnection();
        PreparedStatement pstm = conn.prepareStatement (sql, PreparedStatement.RETURN_GENERATED_KEYS); 
        pstm.setDouble (1, estoque.getQuantidade());
        pstm.setObject (2, TipoMovimentoEstoque.values());
        pstm.setDate (3, (java.sql.Date) estoque.getDataMovto());
        pstm.setLong (4, estoque.getProduto().getIdProduto());
        pstm.setInt (5, estoque.getIdUsuario());
        pstm.setString (6, estoque.getObservacoes());
        pstm.executeUpdate();
        ResultSet rsPK = pstm.getGeneratedKeys();
        if(rsPK.next()){
            return rsPK.getLong(1);
        }
        throw new SQLException("Erro inesperado, item não incluido no pedido");
    }



    /**
     * Utilize PreparedStatement. 
     * Será necessário buscar a quantidade do movimento que está sendo excluído,
     * depois atualizar o estoquesaldo através do método atualizarSaldoEstoque, por último, 
     * faça a exclusão (DELETE) do registro em estoquemovto conforme idMovtoEstoque passado.
     * Transacione essa operação, caso ocorra alguma exceção, faça rollback.
     * @param idMovtoEstoque
     * @return
     */
    @Override
    public boolean excluir(Long idMovtoEstoque) throws SQLException {
        
        throw new UnsupportedOperationException("Não implementado ainda!");
    }



    /**
     * Utilize Statement. 
     * Será necessário buscar a quantidade do movimento que está sendo alterado,
     * caso a quantidade que está no banco de dados for diferente da quantidade
     * passada no atributo movtoEstoque.getQuantidade(), é necessário atualizar a tabela
     * estoquesaldo através do método atualizarSaldoEstoque.
     * Após atualização do saldo, alterar (UPDATE) o registro na tabela estoquemovto. 
     * Utilize transação, se qualquer exceção ocorrer, faça o rollback.
	 * Todos os campos são obrigatórios, com exceção dos campos idUsuario e observacoes que podem ou não ser nulos (null).	
     * @param movtoEstoque
     */
    @Override
    public boolean alterar(EstoqueMovimento movtoEstoque) throws SQLException {
        
        if (movtoEstoque == null || movtoEstoque.getIdMovtoEstoque() == null) {
            throw new RuntimeException("Item com idMovtoEstoque igual a nulo.");
        }

        String sql = "UPDATE projeto.estoquemovto\n"
                + "SET\n"
                + "idMovtoEstoque = " + movtoEstoque.getIdMovtoEstoque() + ",\n"
                + "quantidade` = " + movtoEstoque.getQuantidade() + ",\n"
                + "tipoMovto` = " + movtoEstoque.getTipoMovto() + ",\n"
                + "dataMovto` = " + movtoEstoque.getDataMovto() + ",\n"
                + "idProduto` = " + movtoEstoque.getProduto().getIdProduto() + ",\n"
                + "idUsuario` = " + movtoEstoque.getIdUsuario() + ",\n"
                + "observacoes` = " + movtoEstoque.getObservacoes() + ",\n"
                + "WHERE idMovtoEstoque = " + movtoEstoque.getIdMovtoEstoque();
        System.out.println(sql);
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        int registrosAlterados = stm.executeUpdate(sql);
        return (registrosAlterados == 1);

    }
    

    /**
     * Utilizando Statement ou PreparedStatement, a partir do idProduto informado 
     * e dataInicioMovto (considere como DATE apenas), retorne a lista de objetos do tipo EstoqueMovimento.
     * Utilize a função getEstoqueMovimento para fazer o DataBinding dos dados
     * de cada uma das linhas do ResultSet para o objeto EstoqueMovimento.
     * Retorne apenas os registro cujo dataMovto seja maior ou igual a dataInicioMovto.
     * @param idProduto
     */
    public List<EstoqueMovimento> listarPorProduto(Long idProduto, Date dataInicioMovto) throws SQLException {
        //throw new UnsupportedOperationException("Não implementado ainda!");
        List<EstoqueMovimento> lista = new ArrayList<>();
        String sql = "select * from estoquemovto\n"
                + "where idMovtoEstoque = " + idProduto + ",\n"
                + "and dataMovto >= " + dataInicioMovto + ",\n";
        Connection conn = ConexaoDB.getInstance().getConnection();
        ResultSet rs = conn.createStatement().executeQuery(sql);
        EstoqueMovimento estoqueMov;
        while (rs.next()){
            estoqueMov = getEstoqueMovimento(rs);
            lista.add(estoqueMov);
        }
        return lista;        
    }
    
}
