DROP TABLE IF EXISTS `estoque`;
DROP TABLE IF EXISTS `estoquesaldo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estoquesaldo` (
  `idProduto` int(11) NOT NULL,
  `saldo` double NOT NULL,
  PRIMARY KEY (`idProduto`),
  KEY `fkProdutoEstoque_idx` (`idProduto`),
  CONSTRAINT `fkProdutoEstoque` FOREIGN KEY (`idProduto`) REFERENCES `produto` (`idProduto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movtoestoque`
--

DROP TABLE IF EXISTS `movtoestoque`;
DROP TABLE IF EXISTS `estoquemovto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estoquemovto` (
  `idMovtoEstoque` int(11) NOT NULL AUTO_INCREMENT,
  `quantidade` double NOT NULL,
  `tipoMovto` ENUM('E', 'S') NOT NULL DEFAULT 'E', 
  `dataMovto` timestamp NULL DEFAULT NULL,
  `idProduto` int(11) NOT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `observacoes` longtext,
  PRIMARY KEY (`idMovtoEstoque`),
  KEY `FK_PROD_MOVTOEST_idx` (`idProduto`),
  KEY `FK_USU_MOVTOEST_idx` (`idUsuario`),
  CONSTRAINT `FK_PROD_MOVTOEST` FOREIGN KEY (`idProduto`) REFERENCES `produto` (`idProduto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_USU_MOVTOEST` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
