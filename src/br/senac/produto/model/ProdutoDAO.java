/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.produto.model;

import br.senac.componente.model.BaseDAO;
import br.senac.componentes.db.ConexaoDB;
import br.senac.grupoproduto.model.GrupoProduto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Aluno
 */
public class ProdutoDAO implements BaseDAO<Produto, Long>{

    public static void main(String[] args) throws SQLException {
        ProdutoDAO dao = new ProdutoDAO();
        Produto produto = new Mercadoria();
        GrupoProduto grupoProd = new GrupoProduto();
        grupoProd.setIdGrupoProduto(1);
        
        produto.setNomeProduto("Massa");
        produto.setDataCriacao(new Date());
        produto.setPercICMS(10.5F);
        produto.setTipoProduto(TipoProduto.MERCADORIA);
        produto.setGrupoProduto(grupoProd);
        Long idProduto = dao.inserir(produto);
        System.out.println(idProduto);
        
        produto.setIdProduto(idProduto);
        produto.setDataAlteracao(new Date());
        produto.setNomeProduto("Massa 1");
        dao.alterar(produto);
    }
    
    @Override
    public Produto getPorId(Long id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /*Statement*/
    @Override
    public Long inserir(Produto produto) throws SQLException {
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sql = "INSERT INTO `projeto`.`produto`\n" +
                "(`nomeProduto`,`tipo`,`percICMS`,\n" +
                "`dataCriacao`,\n" +
                "`idgrupoproduto`)\n" +
                "VALUES\n" +
                "('"+ produto.getNomeProduto() +"',"
                + ""+ produto.getTipoProduto().getId()  +","
                + ""+ produto.getPercICMS() +","
                + "{ts '"+ sdf.format(produto.getDataCriacao())   +"'},";
        if(produto.getGrupoProduto() == null)
            sql += " null)";
        else    
            sql += " "+ produto.getGrupoProduto().getIdGrupoProduto()  +")";
        System.out.println(sql);
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        int regInseridos = stm.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
       
        ResultSet rsChaveGerada = stm.getGeneratedKeys();
        rsChaveGerada.next(); //Primeira linha
        return rsChaveGerada.getLong(1);
    }

    @Override
    public boolean excluir(Long id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean alterar(Produto produto) throws SQLException {
        
        String sql = "UPDATE `projeto`.`produto`\n" +
                    "SET\n" +
                    "`nomeProduto` = ?,\n" +
                    "`tipo` = ?,\n" +
                    "`percICMS` = ?,\n" +
                    "`dataAlteracao` = ?,\n" +
                    "`idgrupoproduto` = ?\n" +
                    "WHERE `idProduto` = ?";
        Connection conn = ConexaoDB.getInstance().getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, produto.getNomeProduto());
        ps.setObject(2, produto.getTipoProduto().getId()); //int
        ps.setFloat(3, produto.getPercICMS()); //float
        ps.setTimestamp(4, new java.sql.Timestamp(produto.getDataAlteracao().getTime()));
        //ps.setDate(4, new java.sql.Date(produto.getDataAlteracao().getTime()));
        if(produto.getGrupoProduto() == null)
            ps.setObject(5, null);
        else
            ps.setObject(5, produto.getGrupoProduto().getIdGrupoProduto());
        ps.setObject(6, produto.getIdProduto());
        int regAlterados = ps.executeUpdate();
        return (regAlterados == 1);
    }
    
}
