/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.produto.model;


public class MateriaPrima extends Produto implements Cloneable{
    
    private Long idMateriaPrima;
    private Float percIPI;

    public Long getIdMateriaPrima() {
        return idMateriaPrima;
    }

    public void setIdMateriaPrima(Long idMateriaPrima) {
        this.idMateriaPrima = idMateriaPrima;
        setIdProduto(idMateriaPrima);
    }

    public Float getPercIPI() {
        if(percIPI == null)
            return 0F;
        
        return percIPI;
    }

    public void setPercIPI(Float percIPI) {
        this.percIPI = percIPI;
    }

    @Override
    public Float getTotalPercImposto() {
        return getPercICMS() + getPercIPI();
    }
    
    
    
}
