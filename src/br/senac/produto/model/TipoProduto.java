/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.produto.model;

/**
 *
 * @author Aluno
 */
public enum TipoProduto {
    
    MERCADORIA(1), SERVICO(2), MATERIA_PRIMA(3);
    
    private final Integer id;
    
    private TipoProduto(Integer id){
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
